import DocumentTitle from "react-document-title";
import {
  CORTINAS_CAPRICCIO,
  CORTINAS_ENROLLABLES,
  CORTINAS_PERMAS,
  CORTINAS_ROMANAS,
  CORTINAS_TRIPLE_SHADE,
  CORTINAS_ZEBRAS,
  CORTINA_DESLIZANTES,
  DECOLAMI_TITLE,
  CORTINAS_VENECIANAS,
  CORTINAS_VENECIANAS_DESCRIPCION,
  CORTINAS_ENROLLABLES_DESCRIPCION,
  CORTINAS_ROMANAS_DESCRIPCION,
  CORTINAS_CAPRICCIO_DESCRIPCION,
  CORTINAS_ZEBRA_DESCRIPCION,
  CORTINAS_PERMAS_DESCRIPCION,
  CORTINAS_TRIPLE_SHADE_DESCRIPCION,
  CORTINAS_DESLIZANDTE_DESCRIPCION
} from "../constants/Constants";
import Gallery from "../Gallery/Gallery";

const cortinas = () => {
  return (
    <div className="container">
      <div className="row">
        <div class="mx-auto">

          <DocumentTitle title={DECOLAMI_TITLE + "Persianas"}></DocumentTitle>


          <DocumentTitle title={DECOLAMI_TITLE + "Cortinas"}></DocumentTitle>
          <h3 className="text-center p-4">
            Cortinas <span className="badge badge-secondary">Enrollables</span>
          </h3>
          <p className="pl-5 pr-5 lead">{CORTINAS_ENROLLABLES_DESCRIPCION}</p>

          <Gallery elements={CORTINAS_ENROLLABLES}></Gallery>

          <div style={{ borderTop: "2px solid #808080 ", marginLeft: 20, marginRight: 20 }}></div>

          <h3 className="text-center p-4">
            Cortinas <span className="badge badge-secondary">Cortinas Romanas</span>
          </h3>
          <p className="pl-5 pr-5 lead">{CORTINAS_ROMANAS_DESCRIPCION}</p>

          <Gallery elements={CORTINAS_ROMANAS}></Gallery>

          <div style={{ borderTop: "2px solid #808080 ", marginLeft: 20, marginRight: 20 }}></div>

          <h3 className="text-center p-4">
            Cortinas <span className="badge badge-secondary">Capriccio</span>
          </h3>
          <p className="pl-5 pr-5 lead">{CORTINAS_CAPRICCIO_DESCRIPCION}</p>

          <Gallery elements={CORTINAS_CAPRICCIO}></Gallery>

          <div style={{ borderTop: "2px solid #808080 ", marginLeft: 20, marginRight: 20 }}></div>
          <h3 className="text-center p-4">
            Cortinas <span className="badge badge-secondary">Zebra</span>
          </h3>
          <p className="pl-5 pr-5 lead">{CORTINAS_ZEBRA_DESCRIPCION}</p>

          <Gallery elements={CORTINAS_ZEBRAS}></Gallery>

          <div style={{ borderTop: "2px solid #808080 ", marginLeft: 20, marginRight: 20 }}></div>

          <h3 className="text-center p-4">
            Cortinas Venecianas <span className="badge badge-secondary">Venecianas</span>
          </h3>
          <p className="pl-5 pr-5 lead">{CORTINAS_VENECIANAS_DESCRIPCION}</p>

          <Gallery elements={CORTINAS_VENECIANAS}></Gallery>

          <div style={{ borderTop: "2px solid #808080 ", marginLeft: 20, marginRight: 20 }}></div>

          <h3 className="text-center p-4">
            Cortinas <span className="badge badge-secondary">Permas</span>
          </h3>
          <p className="pl-5 pr-5 lead">{CORTINAS_PERMAS_DESCRIPCION}</p>
          <Gallery elements={CORTINAS_PERMAS}></Gallery>

          <div style={{ borderTop: "2px solid #808080 ", marginLeft: 20, marginRight: 20 }}></div>

          <h3 className="text-center p-4">
            Cortinas <span className="badge badge-secondary">Triple Shade</span>
          </h3>
          <p className="pl-5 pr-5 lead">{CORTINAS_TRIPLE_SHADE_DESCRIPCION}</p>
          <Gallery elements={CORTINAS_TRIPLE_SHADE}></Gallery>

          <div style={{ borderTop: "2px solid #808080 ", marginLeft: 20, marginRight: 20 }}></div>

          <h3 className="text-center p-4">
            Cortinas <span className="badge badge-secondary">Deslizante</span>
          </h3>
          <p className="pl-5 pr-5 lead">{CORTINAS_DESLIZANDTE_DESCRIPCION}</p>
          <Gallery elements={CORTINA_DESLIZANTES}></Gallery>
        </div>
      </div>
    </div>
  );
};

export default cortinas;
