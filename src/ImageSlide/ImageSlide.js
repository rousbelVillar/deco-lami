import { Component } from "react";
import React from "react";
import Carousel from "react-bootstrap/Carousel";
import { CAROUSEL } from "../constants/Constants";
import { CarouselItem } from "react-bootstrap";

class ImageSlide extends Component {
  render() {
    return (
      <div>
        <Carousel>{this.showCarousel()}</Carousel>
      </div>
    );
  }

  showCarousel() {
    const carouselItems = CAROUSEL.map((carouselElement) => (
      <Carousel.Item key={carouselElement.id} interval={3000}>
        <img
          className="d-block w-100"
          src={carouselElement.img}
          alt="First slide"
        />
        {/* <Carousel.Caption>
          <h3>{carouselElement.title}</h3>
          <p>{carouselElement.description}</p>
        </Carousel.Caption> */}
      </Carousel.Item>
    ));
    return carouselItems;
  }
}

export default ImageSlide;
