
import phoneLogo from "../assets/img/phone-icon.png";
import locationLogo from "../assets/img/location-icon.png";
import emailLogo from "../assets/img/email-icon.png";
import persianasVenecianas from "./ImageLinksPersianaVeneciana.js";
import laminadosDeorativos from "./ImageLinksLaminados.js";
import laminadosDeProteccionSolar from "./ImageLinksLamSolar.js";
import cortinasEnrollables from "./ImageLinksCortinasEnrollables.js";
import cortinasNeoluxZebras from "./ImageLinksCortinasZebra";
import cortinasCapriccio from "./ImageLinksCortinasCapriccio.js";
import cortinasPermas from "./ImageLinksCortinasPermas.js";
import cortinasRom from "./ImageLinksCortinasRomanas.js";
import cortinasTrip from "./ImageLinksCortinasTripleShade";
import cortinasDeslizantes from "./ImageLinksCortinasDeslizantes";
import MallasProteccion from "./ImageLinksMallas"
import carousel from "./ImageLinksCarousel";
export const DECOLAMI_TITLE = "Decolami RD | ";
export const DESCRIPCION_MALLAS =
  "Las mallas o redes de seguridad son una excelente opción como sistema de prevención anti-caídas, dejando pasar la luz y el aire y no altera la estética, ni provoca sensación de encierro. " +
  "Son muchas las ventajas que ofrecen estas mallas de protección, iniciando con la tranquilidad que nos brindan poder tener a nuestros niños jugando en su zona preferida de la casa. " +
  "Las mallas de protección constituyen un sistema de protección pasiva compuesto de una red y un marco de anclaje perimetral con piezas de acero resistentes al óxido. " +
  "Se instalan de manera que queden bien tensada para hacerlas pasar lo más desapercibidas posible sin que afecten la estética arquitectónica adaptándose perfectamente del espacio. Pueden ser utilizadas tanto en espacios exteriores como balcones, terrazas o azoteas, ventanas o en interior como en el caso de la escalera."

export const LAMINADOS_PROTECCION_SOLAR_DESCRIPCION =
  "Disponemos de una gran variedad de láminas de control solar para disminución de calor, reducción de resplandor y protección contra los dañinos rayos ultravioleta.  " +
  "También contamos con láminas decorativas y otras láminas especializadas."

export const LAMINADOS_DECORATIVOS_DESCRIPCION =
  "Laminas decorativas para todos tipos de cristales interiores: divisiones, mampara,puertas etc.disponible en sólidos, colores y texturas como rayas, cuadros etc."

export const CORTINAS_VENECIANAS_DESCRIPCION =
  "Las cortinas de madera o cortinas venecianas brindan un toque cálido y tradicional a cualquier espacio interior residencial, hospitalario o corporativo con un excelente control gradual de luz."
export const CORTINAS_ENROLLABLES_DESCRIPCION =
  "Las cortinas enrollables cortinas black out y traslucidas son la opción moderna y práctica en el recubrimiento de ventanas tanto para usos residenciales como comerciales. Las cortinas enrollables le permiten elegir entre una gran variedad de telas con diferentes grados de transmisión de luz."
export const CORTINAS_ROMANAS_DESCRIPCION =
  "Las cortinas romanas combinan maravillosas telas con el movimiento elegante ya sea de sus líneas en formas de lágrima o sus líneas planas, para dar a cualquier ventana un toque de estilo y lujo. " +
  "Controlan la luz sin esfuerzo, mientras lucen sus líneas exactas de sus hermosas telas. " +
  "También pueden ser moldeadas con fibras blackout para brindar oscuridad total. En Persianas Decorativas proporcionamos una amplia gama de telas para brindar cualquier tema de diseño para una ventana bien vestida. "
export const CORTINAS_ZEBRA_DESCRIPCION =
  "Las cortinas Neolux Zebra proveen un excelente manejo gradual de luz, ya sea que desee privacidad total, parcial o gran cantidad de luz y visibilidad." +
  "Con barras translúcidas y opacas horizontales alternadas, la cortina Neolux logrará un efecto de privacidad y transparencia como ninguna otra opción." +
  "Todos nuestros modelos Neolux son parte de la línea de mecanismos Vertilux por lo que están garantizados en su calidad y durabilidad de hasta 10 años."
export const CORTINAS_CAPRICCIO_DESCRIPCION =
  "Las cortinas capriccio es un tipo de diferente de la línea Neolux Zebra, de forma sumamente elegante y vistosa, super moderna y llamativa, las confeccionamos de manera automatizadas a control remoto y también de manipulación manual de acción con cadena, disponibles en diferentes colores para combinar adecuadamente la decoración interna de su hogar u oficina, la capriccio zebra son fabricadas a la medidas del hueco hasta un ancho máximo de 2.90 centímetros, pueden ser instaladas tanto fuera como dentro del hueco de su ventana."
export const CORTINAS_PERMAS_DESCRIPCION =
  "Las cortinas perma de exterior o cortinas enrollables de exterior son la solución al sol y la lluvia para el balcón, terraza o gazebo." +
  "Son fabricadas en Visión Solar Screen de alta resistencia, el cual bloquea el agua de lluvia y reduce los rayos solares en hasta un 80%. " +
  "Este tejido es fuego retardante y antimicrobial. También le permitirá la visión al exterior y sus microperforaciones también le permitirán respirar, manteniendo su área fresca y protegida."
export const CORTINAS_TRIPLE_SHADE_DESCRIPCION =
  "Es una cortina de alta calidad y diseño de vanguardia, por sus tres dimensiones que permiten una mejor decoración. " +
  "Sus suaves láminas de tela suspendidas entre dos velos traslúcidos aportan elegancia y sofisticación a los espacios, combinando con cualquier estilo de decoración y entregando a la vez transparencia y control de privacidad.​"
export const CORTINAS_DESLIZANDTE_DESCRIPCION =
  "Es una cortina especialmente diseñada, una solución perfecta cuando se trata de ambientar grandes ventanales y puertas corredizas. Una forma ideal de separar espacios de manera original y elegante." +
  " Se conforma de varios lienzos o paneles independientes que se traslapan entre sí al correr sobre un riel plano, los cuales se pueden fabricar con diferentes tipos de telas o tejidos."
export const SOBRE_NOSOTROS = [
  {
    title: "NUESTRA EMPRESA",
    paragraphs: [
      {
        p:
          "Somos un equipo joven dedicado al diseño, manufactura y comercialización de productos de terminación y la decoración de interiores. Nuestro compromiso se basa en trabajar siempre a partir de dos fundamentos: productos de muy alta calidad y un servicio al cliente excepcional. ",
      },
      {
        p:
          "Para esto partimos del buen asesoramiento y diseño de cada producto para que cumpla las funciones que satisfacen las necesidades de nuestros clientes. También crear y promover canales para facilitar la comunicación con el cliente y mejorar las formas en que el cliente satisface sus necesidades.",
      },
    ],
  },

  {
    title: "NUESTRA HISTORIA",
    paragraphs: [
      {
        p:
          "Iniciamos operaciones en 2017 con la fabricación y venta de cortinas venecianas, enrollables, zebras, entre otras soluciones, brindando desde hace más de tres años nuestro servicios de los productos Vertilux en toda la República Dominicana.",
      },
      {
        p:
          "En el año 2018 como complemento a los recubrimientos de ventanas agregamos las láminas de control solar de alto rendimiento y como una alternativa para el ahorro de energía y control de la luminosidad donde no se desean recubrimientos tradicionales. ",
      },
    ],
  },
];
export const SERVICIOS = [
  { name: "Mallas", path: "/mallas" },
  { name: "Laminados", path: "/laminados" },
  { name: "Cortinas", path: "/cortinas" },
];

export const COMPANY = [
  { name: "Sobre Nosotros", path: "/sobreNosotros" },
  { name: "Contactanos", path: "/contactUs" },
];

export const CONTACT_PANEL = [
  {
    img: locationLogo,
    description:
      "Proyecto 26 de Enero, C/ Sanchez Edif. 6, Santo Domingo Norte, Los Guaricanos",
    alt: "Location Icon", className: "footerContact"
  },
  { img: phoneLogo, description: "829-889-5209", alt: "Phone Icon", className: "footerContact" },
  { img: emailLogo, description: "decolamird@gmail.com", alt: "Email Icon", className: "footerContact" },
];


export const PHONE_CONTACT = [
  { img: phoneLogo, description: "829-889-5209", alt: "Phone Icon", className: "contactUs" },
];

export const CORTINAS_VENECIANAS = [
  {
    img: persianasVenecianas[0],
    title: "Persianas Venecianas",
    text: "Persiana Veneciana madera",
    id: "persianasVenecianas0",
  },
  {
    img: persianasVenecianas[1],
    title: "Persianas Venecianas",
    text: "Persiana Veneciana madera",
    id: "persianasVenecianas1",
  },
  {
    img: persianasVenecianas[2],
    title: "Persianas Venecianas",
    text: "Persiana Veneciana madera",
    id: "persianasVenecianas2",
  },
];

export const LAMINADOS = [
  {
    img: laminadosDeorativos[0],
    title: "Laminados Decorativos",
    text: "Laminados con fines de decoración",
    id: "laminadosDecorativos0",
  },
  // {
  //   img: laminadosDeorativos[1],
  //   title: "Laminados Decorativos",
  //   text: "Laminados con fines de decoración",
  //   id: "laminadosDecorativos1",
  // },
  {
    img: laminadosDeorativos[2],
    title: "Laminados Decorativos",
    text: "Laminados con fines de decoración",
    id: "laminadosDecorativos2",
  },
  {
    img: laminadosDeorativos[3],
    title: "Laminados Decorativos",
    text: "Laminados con fines de decoración",
    id: "laminadosDecorativos3",
  },


];

export const LAMINADOS_PROTECCION_SOLAR = [
  {
    img: laminadosDeProteccionSolar[0],
    title: "Laminados De Proteccion Solar",
    text: "Laminados con fines de protección solar",
    id: "laminadosProteccionSolar0",
  },
  {
    img: laminadosDeProteccionSolar[1],
    title: "Laminados De Proteccion Solar",
    text: "Laminados con fines de protección solar",
    id: "laminadosProteccionSolar1",
  },
  {
    img: laminadosDeProteccionSolar[2],
    title: "Laminados De Proteccion Solar",
    text: "Laminados con fines de protección solar",
    id: "laminadosProteccionSolar2",
  },
];

export const CORTINAS_ENROLLABLES = [
  {
    img: cortinasEnrollables[0],
    title: "Cortinas Enrollables",
    text: "Cortinas Blackout",
    id: "cortinas",
  },
  {
    img: cortinasEnrollables[1],
    title: "Cortinas Enrollables",
    text: "Cortinas Blackout",
    id: "cortinas",
  },
  {
    img: cortinasEnrollables[2],
    title: "Cortinas Enrollables",
    text: "Cortinas Blackout",
    id: "cortinas",
  },
  // {
  //   img: cortinasEnrollables[3],
  //   title: "Cortinas Enrollables",
  //   text: "Cortinas Blackout",
  //   id: "cortinas",
  // },
  // {
  //   img: cortinasEnrollables[4],
  //   title: "Cortinas Enrollables",
  //   text: "Cortinas Blackout",
  //   id: "cortinas",
  // },
  // {
  //   img: cortinasEnrollables[5],
  //   title: "Cortinas Enrollables",
  //   text: "Cortinas Blackout",
  //   id: "cortinas",
  // },
  // {
  //   img: cortinasEnrollables[6],
  //   title: "Cortinas Enrollables",
  //   text: "Cortinas Blackout",
  //   id: "cortinas",
  // },
  // {
  //   img: cortinasEnrollables[7],
  //   title: "Cortinas Enrollables",
  //   text: "Cortinas Blackout",
  //   id: "cortinas",
  // },
  // {
  //   img: cortinasEnrollables[8],
  //   title: "Cortinas Enrollables",
  //   text: "Cortinas Blackout",
  //   id: "cortinas",
  // },
  // {
  //   img: cortinasEnrollables[9],
  //   title: "Cortinas Enrollables",
  //   text: "Cortinas Blackout",
  //   id: "cortinas",
  // },
  // {
  //   img: cortinasEnrollables[10],
  //   title: "Cortinas Enrollables",
  //   text: "Cortinas Blackout",
  //   id: "cortinas",
  // },
  // {
  //   img: cortinasEnrollables[11],
  //   title: "Cortinas Enrollables",
  //   text: "Cortinas Blackout",
  //   id: "cortinas",
  // },
];

export const CORTINAS_CAPRICCIO = [
  {
    img: cortinasCapriccio[0],
    title: "Cortinas Capriccio",
    text: "Cortinas Capriccio",
    id: "capriccio",
  },
  {
    img: cortinasCapriccio[1],
    title: "Cortinas Capriccio",
    text: "Cortinas Capriccio",
    id: "capriccio",
  },
  {
    img: cortinasCapriccio[2],
    title: "Cortinas Capriccio",
    text: "Cortinas Capriccio",
    id: "capriccio",
  },
  // {
  //   img: cortinasCapriccio[3],
  //   title: "Cortinas Capriccio",
  //   text: "Cortinas Capriccio",
  //   id: "capriccio",
  // },
];

export const CORTINAS_ZEBRAS = [
  {
    img: cortinasNeoluxZebras[0],
    title: "Cortinas Zebra",
    text: "Cortinas Zebra",
    id: "zebra",
  },
  // {
  //   img: cortinasNeoluxZebras[1],
  //   title: "Cortinas Zebra",
  //   text: "Cortinas Zebra",
  //   id: "zebra",
  // },
  // {
  //   img: cortinasNeoluxZebras[2],
  //   title: "Cortinas Zebra",
  //   text: "Cortinas Zebra",
  //   id: "zebra",
  // },
  // {
  //   img: cortinasNeoluxZebras[3],
  //   title: "Cortinas Zebra",
  //   text: "Cortinas Zebra",
  //   id: "zebra",
  // },
  // {
  //   img: cortinasNeoluxZebras[4],
  //   title: "Cortinas Zebra",
  //   text: "Cortinas Zebra",
  //   id: "zebra",
  // },
  // {
  //   img: cortinasNeoluxZebras[5],
  //   title: "Cortinas Zebra",
  //   text: "Cortinas Zebra",
  //   id: "zebra",
  // },
  {
    img: cortinasNeoluxZebras[6],
    title: "Cortinas Zebra",
    text: "Cortinas Zebra",
    id: "zebra",
  },
  {
    img: cortinasNeoluxZebras[7],
    title: "Cortinas Zebra",
    text: "Cortinas Zebra",
    id: "zebra",
  },
];

export const CORTINAS_PERMAS = [
  {
    img: cortinasPermas[0],
    title: "Cortinas Permas",
    text: "Cortinas Permas",
    id: "Permas",
  },
  {
    img: cortinasPermas[1],
    title: "Cortinas Permas",
    text: "Cortinas Permas",
    id: "Permas",
  },
  {
    img: cortinasPermas[2],
    title: "Cortinas Permas",
    text: "Cortinas Permas",
    id: "Permas",
  },
  // {
  //   img: cortinasPermas[3],
  //   title: "Cortinas Permas",
  //   text: "Cortinas Permas",
  //   id: "Permas",
  // },
  // {
  //   img: cortinasPermas[4],
  //   title: "Cortinas Permas",
  //   text: "Cortinas Permas",
  //   id: "Permas",
  // },
  // {
  //   img: cortinasPermas[5],
  //   title: "Cortinas Permas",
  //   text: "Cortinas Permas",
  //   id: "Permas",
  // },
  // {
  //   img: cortinasPermas[6],
  //   title: "Cortinas Permas",
  //   text: "Cortinas Permas",
  //   id: "Permas",
  // },
];

export const CORTINAS_ROMANAS = [
  {
    img: cortinasRom[0],
    title: "Cortinas Romanas",
    text: "Cortinas Romanas",
    id: "Romanas",
  },
  {
    img: cortinasRom[1],
    title: "Cortinas Romanas",
    text: "Cortinas Romanas",
    id: "Romanas",
  },
  {
    img: cortinasRom[3],
    title: "Cortinas Romanas",
    text: "Cortinas Romanas",
    id: "Romanas",
  },
  // {
  //   img: cortinasRom[2],
  //   title: "Cortinas Romanas",
  //   text: "Cortinas Romanas",
  //   id: "Romanas",
  // },
  // {
  //   img: cortinasRom[4],
  //   title: "Cortinas Romanas",
  //   text: "Cortinas Romanas",
  //   id: "Romanas",
  // },
  // {
  //   img: cortinasRom[5],
  //   title: "Cortinas Romanas",
  //   text: "Cortinas Romanas",
  //   id: "Romanas",
  // },
  // {
  //   img: cortinasRom[6],
  //   title: "Cortinas Romanas",
  //   text: "Cortinas Romanas",
  //   id: "Romanas",
  // },
];

export const CORTINAS_TRIPLE_SHADE = [
  {
    img: cortinasTrip[0],
    title: "Cortinas Triple Shade",
    text: "Cortinas Triple Shade",
    id: "Triple Shade",
  },
  {
    img: cortinasTrip[1],
    title: "Cortinas Triple Shade",
    text: "Cortinas Triple Shade",
    id: "Triple Shade",
  },
  {
    img: cortinasTrip[2],
    title: "Cortinas Triple Shade",
    text: "Cortinas Triple Shade",
    id: "Triple Shade",
  },
  // {
  //   img: cortinasTrip[3],
  //   title: "Cortinas Triple Shade",
  //   text: "Cortinas Triple Shade",
  //   id: "Triple Shade",
  // },
  // {
  //   img: cortinasTrip[4],
  //   title: "Cortinas Triple Shade",
  //   text: "Cortinas Triple Shade",
  //   id: "Triple Shade",
  // },
  // {
  //   img: cortinasTrip[5],
  //   title: "Cortinas Triple Shade",
  //   text: "Cortinas Triple Shade",
  //   id: "Triple Shade",
  // },
  // {
  //   img: cortinasTrip[6],
  //   title: "Cortinas Triple Shade",
  //   text: "Cortinas Triple Shade",
  //   id: "Triple Shade",
  // },
];

export const CORTINA_DESLIZANTES = [
  {
    img: cortinasDeslizantes[0],
    title: "Cortinas Deslizantes",
    text: "Cortinas Deslizantes",
    id: "deslizante",
  },
  {
    img: cortinasDeslizantes[1],
    title: "Cortinas Deslizantes",
    text: "Cortinas Deslizantes",
    id: "deslizante",
  },
  {
    img: cortinasDeslizantes[2],
    title: "Cortinas Deslizantes",
    text: "Cortinas Deslizantes",
    id: "deslizante",
  },
  // {
  //   img: cortinasDeslizantes[3],
  //   title: "Cortinas Deslizantes",
  //   text: "Cortinas Deslizantes",
  //   id: "deslizante",
  // },
  // {
  //   img: cortinasDeslizantes[4],
  //   title: "Cortinas Deslizantes",
  //   text: "Cortinas Deslizantes",
  //   id: "deslizante",
  // },
  // {
  //   img: cortinasDeslizantes[5],
  //   title: "Cortinas Deslizantes",
  //   text: "Cortinas Deslizantes",
  //   id: "deslizante",
  // },
  // {
  //   img: cortinasDeslizantes[6],
  //   title: "Cortinas Deslizantes",
  //   text: "Cortinas Deslizantes",
  //   id: "deslizante",
  // },
];

export const MALLAS = [
  {
    img: MallasProteccion[0],
    title: "Mallas de Protección",
    text: "Mallas de Protección",
    id: "mallas1",
  },
  {
    img: MallasProteccion[1],
    title: "Mallas de Protección",
    text: "Mallas de Protección",
    id: "mallas2",
  },
  {
    img: MallasProteccion[2],
    title: "Mallas de Protección",
    text: "Mallas de Protección",
    id: "mallas3",
  },
]

export const CATEGORIAS = [
  {
    img: MallasProteccion[2],
    link: "/mallas",
    title: "Mallas",
    text: "Protección, anticaídas, balcon y ventanas",
    id: "persianas",
  },
  {
    img: laminadosDeorativos[0],
    link: "/laminados",
    title: "Laminados",
    text: "Tenemos Todo tipo de Laminados",
    id: "laminados",
  },
  {
    img: cortinasEnrollables[0],
    link: "/cortinas",
    title: "Cortinas",
    text: "Hermosas Cortinas",
    id: "cortinas",
  },
];

export const CARD_EXAMPLE = [
  {
    img: cortinasNeoluxZebras[3],
    title: "Cortinas Zebra",
    text: "Cortinas Zebra",
    id: "zebra",
    link: "/cortinas",
  },
  {
    img: cortinasPermas[3],
    title: "Cortinas Permas",
    text: "Cortinas Permas",
    id: "Permas",
    link: "/cortinas",
  },
  {
    img: MallasProteccion[1],
    title: "Mallas de Protección",
    text: "Mallas de Protección",
    id: "mallas2",
    link: "/mallas",
  },

  {
    img: cortinasNeoluxZebras[1],
    title: "Cortinas Zebra",
    text: "Cortinas Zebra",
    id: "zebra",
    link: "/cortinas",
  },
  {
    img: laminadosDeorativos[2],
    title: "Laminados Decorativos",
    text: "Laminados con fines de decoración",
    id: "laminadosDecorativos2",
    link: "/laminados",
  },
  {
    img: cortinasPermas[1],
    title: "Cortinas Permas",
    text: "Cortinas Permas",
    id: "Permas",
    link: "/cortinas",
  },

];

export const CAROUSEL = [
  {
    id: "0",
    img: carousel[0],
    title: "SLIDE",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    id: "1",
    img: carousel[1],
    title: "SLIDE",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    id: "2",
    img: carousel[2],
    title: "SLIDE",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    id: "3",
    img: carousel[3],
    title: "SLIDE",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    id: "4",
    img: carousel[4],
    title: "SLIDE",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    id: "5",
    img: carousel[5],
    title: "SLIDE",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    id: "6",
    img: carousel[6],
    title: "SLIDE",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    id: "7",
    img: carousel[7],
    title: "SLIDE",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
];

export const EMAIL_DATA =
{
  service: 'service_4f8k4f9',
  template: 'template_kdh2deb',
  user: 'user_kmUREJ71BVyg78IPYJ6jP'
};

