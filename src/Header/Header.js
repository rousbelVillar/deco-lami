import Navbar from "react-bootstrap/NavBar";
import Nav from "react-bootstrap/Nav";
import decolamiLogo from "../assets/img/logo-decolami.png";
import { LinkContainer } from "react-router-bootstrap";
import DecoRoute from "../constants/DecoRoute";
import "./Header.css";

const header = () => {
  return (
    <div>
      <Navbar expand="lg">
        <LinkContainer to="/inicio">
          <Navbar.Brand className="flex-grow-1">
            <img
              src={decolamiLogo}
              width="250"
              height="auto"
              className="d-inline-block align-top"
              href="/inicio"
              alt="Deco Lami logo"
            />
          </Navbar.Brand>
        </LinkContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <DecoRoute path="/inicio" text="Inicio"></DecoRoute>
            <DecoRoute path="/mallas" text="Mallas"></DecoRoute>
            <DecoRoute path="/laminados" text="Laminados"></DecoRoute>
            <DecoRoute path="/cortinas" text="Cortinas"></DecoRoute>
            <DecoRoute path="/sobreNosotros" text="Sobre Nosotros"></DecoRoute>
            <DecoRoute path="/contactUs" text="Contáctanos"></DecoRoute>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
};

export default header;
