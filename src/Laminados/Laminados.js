import DocumentTitle from "react-document-title";
import {
  DECOLAMI_TITLE,
  LAMINADOS,
  LAMINADOS_PROTECCION_SOLAR,
  LAMINADOS_DECORATIVOS_DESCRIPCION,
  LAMINADOS_PROTECCION_SOLAR_DESCRIPCION,
} from "../constants/Constants.js";
import Gallery from "../Gallery/Gallery";

const laminados = () => {
  return (
    <div className="container">
      <div className="row">
        <div class="mx-auto">
          <DocumentTitle title={DECOLAMI_TITLE + "Laminados"}></DocumentTitle>

          <h3 className="text-center p-4">
            Laminados <span className="badge badge-secondary">Protección Solar</span>
          </h3>
          <p className="pl-5 pr-5 lead">{LAMINADOS_PROTECCION_SOLAR_DESCRIPCION}</p>

          <Gallery elements={LAMINADOS_PROTECCION_SOLAR} />
          <div style={{ borderTop: "2px solid #808080 ", marginLeft: 20, marginRight: 20 }}></div>

          <h3 className="text-center p-4">
            Laminados <span className="badge badge-secondary">Decoración</span>
          </h3>
          <p className="pl-5 pr-5 lead">{LAMINADOS_DECORATIVOS_DESCRIPCION}</p>
          <Gallery elements={LAMINADOS} />

        </div>
      </div>

    </div>
  );
};

export default laminados;
