import { Component } from "react";
import { LinkContainer } from "react-router-bootstrap";

import "./Gallery.css";
class Gallery extends Component {
  render() {
    return (
      <div className="album py-5 ">
        <div className="container">
          <div className="row">{this.showGallery()}</div>
        </div>
      </div>
    );
  }

  showGallery() {
    const cardElements = this.props.elements.map((element) => (
      <div key={element.id} className="col-md-4 ">
        <div className="card mb-4 shadow-sm">
          <img alt={element.title} src={element.img} className="img-top"></img>
          <div className="card-body">
            <h5 className="card-title">{element.title}</h5>
            <p className="card-text">{element.text}</p>
            {this.renderBotton(element.link)}
          </div>
        </div>
      </div>
    ));
    return cardElements;
  }

  renderBotton(link) {
    return link ? (
      <LinkContainer to={link}>
        <a href={link} className="btn btn-dark">
          Detalles
        </a>
      </LinkContainer>
    ) : (
      <div></div>
    );
  }
}
export default Gallery;
