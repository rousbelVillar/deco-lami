import SocialMediaPanel from "./SocialMediaPanel";
import "./Footer.css";
import LinkPanel from "./LinkPanel";
import ContactPanel from "./ContactPanel";
import decolamiLogo from "../assets/img/logo-decolami.png";

import {
  SERVICIOS,
  COMPANY,
  CONTACT_PANEL,
} from "../constants/Constants";
const footer = () => {
  return (
    <footer className="footer bg-light mt-auto">
      <div className="content">
        <div className="common footer_col_description">
          <img
            src={decolamiLogo}
            width="250"
            height="auto"
            className="logo d-inline-block align-top"
            href="#decolamilogo"
            alt="Deco Lami logo"
          />
        </div>
        <div className="common footer_col_social">
          <SocialMediaPanel />
        </div>
        <div className="common footer_col">
          <LinkPanel title="Servicios" links={SERVICIOS} />
        </div>
        <div className="common footer_col">
          <LinkPanel title="Compañia" links={COMPANY} />
        </div>
        <div className="common contact">
          <ContactPanel contacts={CONTACT_PANEL} />
        </div>
      </div>
    </footer>
  );
};

export default footer;
