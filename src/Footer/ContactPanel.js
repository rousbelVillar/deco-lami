import { Component } from "react";
class ContactPanel extends Component {
  render() {
    return (
      <div className="contactPanel">
        <ul>{this.generateContacts()}</ul>
      </div>
    );
  }

  generateContacts = () => {
    const links = this.props.contacts.map((contact) => (
      <div key={contact.alt} className="contact_item">
        <img
          width="20"
          height="20"
          className="mr-1"
          href={"#" + contact.alt}
          alt={contact.alt}
          src={contact.img}
        />
        <div>{contact.description}</div>
      </div>
    ));
    return links;
  };
}

export default ContactPanel;
