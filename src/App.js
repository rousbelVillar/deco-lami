import { Component } from "react";
import Gallery from "./Gallery/Gallery";
import "./App.css";
import {
  CARD_EXAMPLE,
  CATEGORIAS,
  DECOLAMI_TITLE,
} from "./constants/Constants";
import ImageSlide from "./ImageSlide/ImageSlide";
import DocumentTitle from "react-document-title";
class App extends Component {
  render() {
    return (
      <div>
        <DocumentTitle title={DECOLAMI_TITLE + "Inicio"}></DocumentTitle>
        <ImageSlide slides={CARD_EXAMPLE}></ImageSlide>
        <h4 className="text-center p-4">Nuestras Categorias</h4>
        <Gallery elements={CATEGORIAS}></Gallery>
        <h4 className="text-center p-4">
          Nuestras Collecciones | Los Mas Buscados{" "}
        </h4>
        <Gallery elements={CARD_EXAMPLE}></Gallery>
      </div>
    );
  }
}

export default App;
