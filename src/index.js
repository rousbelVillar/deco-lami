import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Contacto from "./Contacto/Contacto";
import Header from "./Header/Header";
import Cortinas from "./Cortinas/Cortinas";
import Laminados from "./Laminados/Laminados";
import SobreNosotros from "./SobreNosotros/SobreNosotros";
import Footer from "./Footer/Footer";
import mallas from "./Mallas/Mallas";
import ScrollRestoration from 'react-scroll-restoration'

const Routing = () => {
  return (
    <Router>
      <ScrollRestoration />
      <Header />
      <Switch className="content">
        <Route path="/inicio" component={App} />
        <Route path="/mallas" component={mallas} />
        <Route path="/laminados" component={Laminados} />
        <Route path="/cortinas" component={Cortinas} />
        <Route path="/sobreNosotros" component={SobreNosotros} />
        <Route path="/contactUs" component={Contacto} />
        <Route path="*">
          <App />
        </Route>
      </Switch>
      <Footer />
    </Router>
  );
};

ReactDOM.render(
  <React.StrictMode>
    <Routing />
  </React.StrictMode>,

  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
